<?php

/**
 * @file
 * Hook implementations and helper functions.
 */

/**
 * Shows admin menu on every page except the listed pages.
 */
define('ADMIN_MENU_VISIBILITY_NOTLISTED', 0);

/**
 * Shows admin menu on only the listed pages.
 */
define('ADMIN_MENU_VISIBILITY_LISTED', 1);

/**
 * Shows admin menu if the associated PHP code returns TRUE.
 */
define('ADMIN_MENU_VISIBILITY_PHP', 2);

/**
 * Implements hook_form_alter().
 */
function admin_menu_visibility_form_admin_menu_theme_settings_alter(&$form, &$form_state) {

  // Per-path visibility.
  $form['admin_menu_visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Visibility'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'advanced',
    '#weight' => 1,
    '#tree' => TRUE,
  );

  $settings = variable_get('admin_menu_visibility', array());
  $settings = array_merge(_admin_menu_visibility_default(), $settings);
  $visibility = $settings['visibility'];
  $pages = $settings['pages'];

  $access = user_access('use PHP for settings');
  if ($visibility == ADMIN_MENU_VISIBILITY_PHP && !$access) {
    $form['admin_menu_visibility']['visibility'] = array(
      '#type' => 'value',
      '#value' => ADMIN_MENU_VISIBILITY_PHP,
    );
    $form['admin_menu_visibility']['pages'] = array(
      '#type' => 'value',
      '#value' => $pages,
    );
  }
  else {
    $options = array(
      ADMIN_MENU_VISIBILITY_NOTLISTED => t('All pages except those listed'),
      ADMIN_MENU_VISIBILITY_LISTED => t('Only the listed pages'),
    );
    $description = t(
      "Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %admin for the administrative overview page and %admin-wildcard for every configuration pages.. %front is the front page.",
      array(
        '%admin' => 'admin',
        '%admin-wildcard' => 'admin/*',
        '%front' => '<front>',
      )
    );

    if (module_exists('php') && $access) {
      $options += array(ADMIN_MENU_VISIBILITY_PHP => t('Pages on which this PHP code returns <code>TRUE</code> (experts only)'));
      $title = t('Pages or PHP code');
      $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    else {
      $title = t('Pages');
    }
    $form['admin_menu_visibility']['visibility'] = array(
      '#type' => 'radios',
      '#title' => t('Show admin menu on specific pages'),
      '#options' => $options,
      '#default_value' => $visibility,
    );
    $form['admin_menu_visibility']['pages'] = array(
      '#type' => 'textarea',
      '#title' => '<span class="element-invisible">' . $title . '</span>',
      '#default_value' => $pages,
      '#description' => $description,
    );
  }
}

/**
 * Implements hook_preprocess_page().
 *
 * Utilize the given function to hide itself.
 */
function admin_menu_visibility_preprocess_page(&$variables) {
  if (!admin_menu_visibility_match()) {
    module_invoke('admin_menu', 'suppress');
  }
}

/**
 * Check page visibility.
 *
 * Check if it is the case to show admin_menu.
 *
 * @return bool
 *   TRUE when the menu shold show in current path.
 */
function admin_menu_visibility_match() {
  // Cache for single page load.
  $page_match = &drupal_static(__FUNCTION__);

  $settings = variable_get('admin_menu_visibility', array());
  $settings = array_merge(_admin_menu_visibility_default(), $settings);
  $visibility = $settings['visibility'];
  $pages = $settings['pages'];

  if (!isset($page_match)) {
    if (!empty($pages)) {
      // Convert path to lowercase. This allows comparison of the same path
      // with different case. Ex: /Page, /page, /PAGE.
      $pages = drupal_strtolower($pages);
      if ($visibility < ADMIN_MENU_VISIBILITY_PHP) {
        // Convert the Drupal path to lowercase.
        $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));
        // Compare the lowercase internal and lowercase path alias (if any).
        $page_match = drupal_match_path($path, $pages);
        if ($path != $_GET['q']) {
          $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
        }
        // When $visibility has a value of 0 (ADMIN_MENU_VISIBILITY_NOTLISTED),
        // the block is displayed on all pages except those listed in $pages.
        // When set to 1 (ADMIN_MENU_VISIBILITY_LISTED), it is displayed only on
        // those pages listed in $pages.
        $page_match = !($visibility xor $page_match);
      }
      elseif (module_exists('php')) {
        $page_match = php_eval($pages);
      }
      else {
        $page_match = FALSE;
      }
    }
    else {
      $page_match = TRUE;
    }
  }

  return $page_match;
}

/**
 * Default variable visibility value.
 */
function _admin_menu_visibility_default() {
  return array(
    'visibility' => ADMIN_MENU_VISIBILITY_NOTLISTED,
    'pages' => '',
  );
}
