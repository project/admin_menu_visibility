CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The Administration Menu Visibility module controls the visibility of
administration menu with paths as what core block does. It provides a
visibility tabs in advance block at the bottom of the configuration page of
Administration Menu module. 

This module will remove all markup, js and css files from the output for the
pages which do not need administration menu.


REQUIREMENTS
------------

This module requires the following module(s):

 * Administration Menu (https://drupal.org/project/admin_menu)


RECOMMENDED MODULES
-------------------

 * Adminimal Administration Menu
   (https://drupal.org/project/adminimal_admin_menu):
   For the pages which the administration menu is opt-out, js and css files from
   this module will also be removed.

 * PHP filter (core)
   This module supports PHP snippet on visibility control.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGRATION
------------

This module has modified Administration Menu settings. A new "Visibility" tab
is added in the advance block at the bottom of the settings page.


MAINTAINERS
-----------

Current maintainers:
 * Jimmy Ko (jimmyko) - https://drupal.org/user/2181737
